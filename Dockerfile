FROM ubuntu:18.04

# Dependencies
RUN apt-get update && apt-get install -y wget ruby vim && gem install httparty

# Get Packages
RUN wget https://github.com/prometheus/prometheus/releases/download/v2.4.3/prometheus-2.4.3.linux-amd64.tar.gz -O prometheus.tar.gz && \
    wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_5.1.4_amd64.deb && \
    apt-get install -y ./grafana_5.1.4_amd64.deb && \
    rm grafana_5.1.4_amd64.deb && \
    mkdir -p prometheus && \
    mkdir -p /etc/prometheus && \
    tar -xzvf ./prometheus.tar.gz --strip-components=1 -C prometheus && \
    cp prometheus/prometheus /usr/local/bin/ && \
    cp -r prometheus/consoles /etc/prometheus && \
    cp -r prometheus/console_libraries /etc/prometheus && \
    rm -rfv prometheus prometheus.tar.gz

COPY rules.yml prograf grafana.rb prometheus.rb /
COPY dashboards /dashboards

EXPOSE 3000/tcp
EXPOSE 9090/tcp

CMD ["/prograf"]
