# frozen_string_literal: true

puts 'Prometheus Setup'

# Create prometheus config
module Prometheus
  def self.setup
    hosts = ENV['hosts'].split(',')

    prometheus = {
      'job_name' => 'prometheus',
      'static_configs' => [
        {
          'targets' => [
            'localhost:9090'
          ]
        }
      ]
    }

    label = [
      {
        'regex' => '(.+):.*',
        'replacement' => '${1}',
        'source_labels' => [
          '__address__'
        ],
        'target_label' => 'host'
      }
    ]

    jobs = [
      { job: 'redis', port: 9121 },
      { job: 'postgres', port: 9187 },
      { job: 'node', port: 9100 },
      { job: 'gitlab-workhorse', port: 9229 },
      { job: 'gitlab-unicorn', port: 8080, metrics_path: '/-/metrics' },
      { job: 'gitlab-sidekiq', port: 8082 },
      { job: 'gitlab_monitor_database', port: 9168, metrics_path: '/database' },
      { job: 'gitlab_monitor_sidekiq', port: 9168, metrics_path: '/sidekiq' },
      { job: 'gitlab_monitor_process', port: 9168, metrics_path: '/process' },
      { job: 'gitaly', port: 9236 }
    ]

    config = {
      'global' => {
        'scrape_interval' => '15s',
        'scrape_timeout' => '15s'
      },
      'rule_files' => ['rules.yml'],
      'remote_read' => [],
      'remote_write' => [],
      'scrape_configs' => []
    }

    config['scrape_configs'].push prometheus

    jobs.each do |data|
      targets = {
        'targets' => hosts.map { |x| "#{x}:#{data[:port]}" }
      }

      job = {
        'job_name' => data[:job],
        'static_configs' => Marshal.load(Marshal.dump(targets)),
        'relabel_configs' => Marshal.load(Marshal.dump(label))
      }

      job['metrics_path'] = data[:metrics_path] if data.key? :metrics_path

      config['scrape_configs'].push Marshal.load(Marshal.dump(job))
    end

    # binding.pry
    prometheus_config = config.to_yaml.gsub(/    targets:/, '  - targets:')
    File.write('prometheus.yml', prometheus_config)
  end

  def self.start
    setup
    spawn('prometheus')
  end
end
