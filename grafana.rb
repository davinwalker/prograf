# frozen_string_literal: true

GRAFANA_URI = 'http://localhost:3000/api'
GRAFANA_HEADERS = { 'Content-Type' => 'application/json' }.freeze
GRAFANA_AUTH = { username: 'admin', password: 'admin' }.freeze
GRAFANA_DATASOURCE = {
  name: 'Prometheus',
  type: 'prometheus',
  url: 'http://localhost:9090',
  access: 'proxy',
  basicAuth: false
}.freeze

puts 'Grafana Setup'

# Grafana
module Grafana
  def self.command
    [
      '/usr/sbin/grafana-server',
      '--pidfile=/var/run/grafana-server.pid',
      '-homepath=/usr/share/grafana',
      '--config=/etc/grafana/grafana.ini',
      'cfg:default.paths.provisioning=/etc/grafana/provisioning',
      'cfg:default.paths.data=/var/lib/grafana',
      'cfg:default.paths.logs=/var/log/grafana'
    ]
  end

  def self.start
    pid = spawn(command.join(' '))
    wait
    configure
    dashboards
    pid
  end

  def self.wait
    loop do
      begin
        break if Socket.tcp('127.0.0.1', 3000, connect_timeout: 15)
      rescue StandardError
        puts 'Waiting for Grafana'
        sleep 3
      end
    end
  end

  def self.configure
    puts 'Configure'
    response = HTTParty.post(
      GRAFANA_URI + '/datasources',
      headers: GRAFANA_HEADERS,
      basic_auth: GRAFANA_AUTH,
      body: GRAFANA_DATASOURCE.to_json
    )

    puts response.code
  end

  def self.dashboards
    Dir[File.join('/dashboards', '*.json')].each do |file|
      puts "Importing #{file}"
      payload  = { dashboard: JSON.parse(File.read(file)), overwrite: true }

      response = HTTParty.post(
        GRAFANA_URI + '/dashboards/db',
        headers: GRAFANA_HEADERS, basic_auth: GRAFANA_AUTH,
        body: JSON.dump(payload)
      )
      # raise 'Derp' unless response.code == 200
      puts response.code
    end
  end
end
