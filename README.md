# Prometheus / Grafana


# Build

```
docker build -t prograf .
docker run -it prograf bash
```

# Run

```
docker run -p 3000:3000 -e "hosts=10.0.0.200,10.0.0.201" prograf
```


Navigate to http://localhost:3000 `admin:admin`

## Build and Run
```
docker build -t prograf . && docker run -e "hosts=10.0.0.200,10.0.0.201" -p 9090:9090 -p 3000:3000 -it prograf
```


# Gitlab Settings

Exporters will need to be available to prometheus

```
prometheus['enable'] = false
unicorn['listen'] = '0.0.0.0'
gitlab_monitor['listen_address'] = '0.0.0.0'
node_exporter['listen_address'] = '0.0.0.0:9100'
gitaly['prometheus_listen_addr'] = '0.0.0.0:9236'
sidekiq['listen_address'] = '0.0.0.0' # gitlab-sidekiq
postgres_exporter['listen_address'] = '0.0.0.0:9187'
redis_exporter['listen_address'] = '0.0.0.0:9121'
gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'
```
